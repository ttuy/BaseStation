#include <gtest/gtest.h>
#include "Config.h"

TEST(BASESTATION, motor_shield_type)
{
	ASSERT_EQ(MOTOR_SHIELD_TYPE, 0);
}

TEST(BASESTATION, max_main_registers)
{
	ASSERT_EQ(MAX_MAIN_REGISTERS, 12);
}

TEST(BASESTATION, comm_interface)
{
	ASSERT_EQ(COMM_INTERFACE, 1);
}

TEST(BASESTATION, ip_address)
{
	int ip_address[] = IP_ADDRESS;
	int ip_address_testing[] = {192, 168, 15, 200};
	for(int i = 0; i < 4; i++){
		ASSERT_EQ(ip_address[i], ip_address_testing[i]);
	}
}

TEST(BASESTATION, ethernet_port)
{
	ASSERT_EQ(ETHERNET_PORT, 80);
}

TEST(BASESTATION, mac_address)
{
	int mac_address[] = MAC_ADDRESS;
	int mac_address_testing[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xEF};
	for(int i = 0; i < 6; i++){
		ASSERT_EQ(mac_address[i], mac_address_testing[i]);
	}
}

TEST(BASESTATION, version)
{
	ASSERT_EQ(VERSION, "2.0.0");
}

int main(int argc, char **argv)
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
