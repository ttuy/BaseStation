# Preparare il setup

- Prendere Arduino Mega e collegare sopra di esso la Ethernet Shield e attaccarci sopra la Motor Shield.
- Collegare con un cavetto il pin 13 con il pin 2.
- Alimentare la Motor Shield con un alimentatore da 16V.
- Collegare il cavo Ethernet.
- Alimentare Arduino Mega con un alimentatore da 5V o da 9V.
- Usare il file fritzing per capire come collegare le parti aggiuntive.
- In caso di problemi di connessione Ethernet premere il bottone di reset.

Per risolvere qualsiasi tipo di problema sembra che il reset della board, fatto con il pulsante di reset di Arduino o quello aggiuntivo, sembra la strada giusta.

Per qualsiasi dubbio si faccia riferimento alla guida su Github: https://github.com/DccPlusPlus/BaseStation/wiki