# BaseStation

Progetto per la gestione di locomotive usando il protocollo dcc. Per visualizzare il progetto originale, dato che questo progetto è un clone del progetto originale più alcune aggiunte, si fa riferimento alla pagina ufficiale: https://github.com/DccPlusPlus/BaseStation.

## Procedure di setup

Vedere il file [SETUP.md](SETUP.md)

## Procedura di debug (Arduino Mega + Ethernet Shield + Motor Shield).

### Testare Arduino Mega.

- togliere la Motor Shield e la Ethernet Shield
- nel file Config.h cambiare la define COMM_INTERFACE = 0.
- caricare il nuovo firmware.
- collegare il pin 13 con il pin 3 (il led 13 dovrebbe spegnersi).
- inviare < 1 > e il led dovrebbe accendersi.
- inviare < 0 > e il led dovrebbe spegnersi.
- riprovare i comandi precedenti più volte.
- inviare il comando < 0 >.
- collegare il pin 13 con il pin 11 (il led 13 dovrebbe spegnersi).
- ripetere i comandi precedenti anche nella nuova configurazione.
- inviare il comando < 0 >.
- collegare il pin 13 con il pin 12 (il led 13 dovrebbe essere acceso).
- inviare il comando < D > (il led 13 dovrebbe lampeggiare).
- collegare il pin 13 con il pin 2 (il led dovrebbe continuare a lampeggiare).

### Testare Arduino Mega + Motor Shield.

- collegare la motor shield ad Arduino Mega dopo aver scollegato il Vin sulla motor shield.
- collegare il pin 13 con il pin 2.
- inviare il comando < 1 > (i led si dovrebbero accendere).
- inviare il comando < 0 > (i led si dovrebbero spegnere).
- inviare il comando < 1 > e successivamente il comando < D > ( i led dovrebbero cominciare i lampeggiare in modo alternato sulle due diverse linee di uscita).

### Testare Arduino Mega + Ethernet Shield + Motor Shield.

- collegare la ethernet shield e la motor shield ad Arduino Mega.
- nel file Config.h cambiare la define COMM_INTERFACE = 1.
- collegare il pin 13 con il pin 2.
- inviare attraverso una connessione di rete (socket) il comando < 1 >, < 0 > e < D > e il risultato dovrebbe essere lo stesso di quello visto in precedenza.

## Downloads

BaseStation: [Scarica](https://gitlab.com/therickys93/BaseStation/-/jobs/artifacts/master/download?job=compile_BaseStation)
