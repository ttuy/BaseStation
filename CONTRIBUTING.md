# Contribuire a BaseStation

## Sei un programmatore?

Per contribuire al progetto di BaseStation bisogna forkare il progetto (creare la nuova feature o risolvere il problema) e poi creare una pull request verso questo repository con il nuovo codice.

Questo modo di procedere potrebbe essere rivisto in futuro.

## Non sei un programmatore?

Se non sei un programmatore condividi pure la tua idea con noi attraverso il Service Desk.
